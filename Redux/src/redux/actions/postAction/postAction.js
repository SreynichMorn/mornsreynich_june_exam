import { GET_POST} from "./postActionType";

export const getPosts = () => {
    Axios.get('http://110.74.194.125:3535/api/articles')
    console.log("======> Get Post");
    return {
      type: GET_POST,
      data: [
        {
          userId: 1,
          id: 1,
          title:
            "Web",
          description:
            "Hello Web",
        },
        {
          userId: 1,
          id: 2,
          title: "Java",
          description:
            "Hello Java",
        },
      ],
    };
  };
  