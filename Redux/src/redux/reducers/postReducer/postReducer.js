import { GET_POST, DELETE_POST } from "../../actions/postAction/postActionType";

const defaultState = {
  data: []
}

export function postReducer(state = defaultState, action) {
  switch(action.type) {
    case GET_POST:
      return {
        ...state,
        data: action.data
      }
 
    default:
      return state
  }
}